
# GitHub Most Starred
 
This application uses the following technologies.

* *Slim* - a PHP micro framework
* *AngularJS 1* - a Javascript framework
* *Bootstrap* - HTML, CSS, and JS framework
* *Font Awesome* - Iconic font and CSS toolkit
* *MySQL* - Database

### Installation Notes
* Simply pull the code down
* Edit the user credentials and hostname in the `github_db.sql` file.
* Run the `github_db.sql` file.
* Update the `api/config.php` file with the correct credentials
* Go to your browser and watch it work