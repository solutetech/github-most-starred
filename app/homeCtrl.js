app.controller('HomeCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {
    
    $scope.getRepos = function() {
        Data.get('get-most-starred').then(function (response) {
            $scope.starred = response;
            $scope.repos = response.data;
        });
    };
    $scope.getRepos();
    
    $scope.populate = function() {
        Data.get('populate-most-starred').then(function (response) {
            Data.toast(response);
            $scope.getRepos();
        });
    };
    
    $scope.viewRepo =function(repo) {
      Data.get('get-repo/' + repo ).then(function (response) {
            $scope.repo = response.data; 
        });  
    };
});