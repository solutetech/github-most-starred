CREATE DATABASE IF NOT EXISTS github;

USE github;

--
-- Table structure for table `most_starred`
--

CREATE TABLE IF NOT EXISTS `most_starred` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repositoryId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `avatar_url` varchar(100) NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(100) NOT NULL,
  `created_date` varchar(30) NOT NULL,
  `last_push_date` varchar(30) NOT NULL,
  `number_of_stars` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE USER 'github_user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON github.* TO 'github_user'@'localhost';