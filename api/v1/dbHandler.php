<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once 'dbConnect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }
    /**
     * Fetching single record
     */
    public function getOneRecord($query) {
        $r = $this->conn->query($query. " limit 1") or die($this->conn->error.__LINE__);
        return $result = $r->fetch(PDO::FETCH_ASSOC);    
    }
    
    /**
     * Fetching all record
     */
    public function getAllRecord($query) {
        $r = $this->conn->query($query) or die($this->conn->error.__LINE__);
        return $result = $r->fetchAll(PDO::FETCH_ASSOC);    
    }
    
    /**
     * Creating new record
     */
    public function insertIntoTable($values, $column_names, $table_name) {
        
        $columns = implode($column_names,',');
        $value = implode($values,',');
        $repoId = $values['id'];
        
        $get_repo = $this->getOneRecord("Select id From most_starred Where repositoryId = $repoId");
       
        if(!$get_repo) {
            
            $query = "INSERT INTO ".$table_name." (".$columns.") VALUES (".$value.")";
            $r = $this->conn->query($query) or die($this->conn->error.__LINE__);
            
            if($r) {
                $repo['added'][$this->conn->insert_id] = $values['name'];
            } else {
                $repo['not-added'][$repoId] = $values['name'];
            }
            
        } else {
            $repo['updated'][$repoId] = $values['name'];
        }
        
        return $query;
    }

}

?>
