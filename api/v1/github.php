<?php
$app->get('/populate-most-starred', function() {
    require 'dbHandler.php';
    $db = new DbHandler();
    $url = "https://api.github.com/search/repositories?q=stars:>5+language:PHP&sort=stars";
    
    // create a new cURL resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "GitHub Most Starred Agent" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $git = curl_exec($ch);
    
    curl_close($ch);
    
    $table_name = "most_starred";
    $column_names = array("repositoryId", "name", "avatar_url", "description", "url", "created_date", "last_push_date", "number_of_stars");
    $repo_names = array("id", "name", "description", "url", "created_at", "pushed_at", "stargazers_count", "owner");
    $repos = json_decode($git, true);
    
    for($i=0; $i<count($repos['items']); $i++) {

        foreach($repos['items'][$i] as $key => $value) {
 
            if(in_array($key, $repo_names)) {
                
                if($key == "id") {
                    $repo[$key] = $value;
                } else
                if($key == "owner") {
                    $avatar = $value['avatar_url'];
                    $repo['avatar_url'] = "'$avatar'";
                } else {
                    $repo[$key] = "'$value'";
                }
            }
        }
        
        $post = $db->insertIntoTable($repo, $column_names, $table_name);
    }
    
    $results['status'] = "success";
    $results['message'] = "Updated list of repos";
    $results['data'] = $post;
    echoResponse(200, $results);
});



$app->get('/get-most-starred', function() {
    require 'dbHandler.php';
    $db = new DbHandler();
    $repos = $db->getAllRecord("select * from most_starred");
    
    $results['status'] = "success";
    $results['total_count'] = count($repos);
    $results['data'] = $repos;
    echoResponse(200, $results);
});


$app->get('/get-repo/:id', function($id) {
    require 'dbHandler.php';
    $db = new DbHandler();
    $repo = $db->getOneRecord("select * from most_starred where repositoryId = $id");
    
    $results['status'] = "success";
    $results['data'] = $repo;
    echoResponse(200, $results);
});

?>